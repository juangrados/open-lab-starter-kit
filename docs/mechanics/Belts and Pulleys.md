# Motion Systems

There are various mechanisms used for transmitting the motion on the X,Y and Z axes. Some of the most commonly used types are:
1)	Rollers (Delrin wheels, metal rollers, bearings etc) rolling either on or within a straight or profiled extrusion (V-Rails)
a.	Delrin wheels rolling within the V-slot grooves of a Aluminium V-slot extrusion profile. These are most commonly used in budget DIY 3D printers, since they are ideally suited for low load applications.
- Pros:
  - Ease of build and assembly
  - Robust and reliable
  - Com
- Cons:
  - Not as smooth as linear rails
  - The Delrin coating on wheel wears away with time

2) Linear rods with linear bearings or simply bronze bushings
3)	Timing Belts are toothed rubber belts reinforced with fibres within the matrix that provide the belts their strength and touchness. Timing belts require matching toothed and idler pulleys for motion. 
4)	A linear rail consists of a stiff, steel rail, along which a carriage slide. Most commonly, the carriage contains recirculating ball bearings that provide contact points between the carriage and the rail, as depicted above. This enables a smooth sliding motion as the balls roll between the surfaces. The shape of the shaft enables the carriage to stay locked on with tight tolerances, restricting the motion to strictly linear directions.
https://i.all3dp.com/cdn-cgi/image/fit=cover,w=1000,gravity=0.5x0.5,format=auto/wp-content/uploads/2019/05/22192229/a-cut-away-depiction-of-a-linear-rail-and-its-rec-linear-motion-tips-190518_download.jpg 
a.	Pros:
i.	High stiffness in all directions other than motion direction
ii.	High precision > Reduced play and binding
iii.	Easy Mounting
b.	Cons:
i.	Fragile i.e. dropping the carriage can result in loss of the recirculating balls causing damage – so must be handled with care
ii.	2 to 3 times more expensive than linear bearings solution
 
5)	Acme Screw with nut block
6)	Ball Screw with nut block
