# Home

## 3D Printer Mechanics
1.	Cartesian – eg. In the Prusa i3 and the Ender 3 whereby the Hot End moves in the X and Z axis, while the bed moves in the Y axis.
(Put picture)
- Pros:
  - Tried and trusted design
  - Very commonly found in printers
  - Easy to build and assemble
  - Kinematics are simple for firmware to calculate making a cheaper 8 bit board sufficient
- Cons:
  - Large Prints can get heavy and difficult for bed to move without causing build defects
  - Bed needs to move in the front and back of machine, so the bed overhangs in front and back substantially. This makes machine footprint much bigger than actual print area
	
2.	**Delta 3D Printers** has 3 stepper motors controlling linear motion up and down the tower, i.e. print head does all the movement while the build plate stays stationary. Therefore the print head, extruder and arms need to be as lightweight as possible. Print speed is also high.
- Pros:
  - Has a small footprint since the build plate is stationary
  - Is ideal for tall prints
  - High accuracy and speed
- Cons:
  - Print head needs X,Y and Z motors to work simultaneously and constantly for every motion – can be more energy intensive
  - Kinematics are difficult for firmware to calculate
  - Inefficiency of height since extra height for machine required to accommodate the arms and print head.
  - Circular build plate can be a disadvantage for some
  - 
3.	**Core XY:** Mix between cartesian and delta in characteristics. Single stepper motor to control the Z-axis moving bed up or down. 2 Stepper motors work to create the X and Y motion. Calculations are tricky and benefit from 32bit mainboard. Also optimal when printhead and extruder have minimal mass
![Core XY Mechanism](https://www.researchgate.net/profile/Shane_Hooper/publication/346036049/figure/fig1/AS:966073641865219@1607341313422/CoreXY-Mechanism_W640.jpg)
- **Pros:**
  - Boxed Frame design commonly found in Core XY is more rigid compared to Prusa i3 style open frame
  - More efficient in Y space usage > High space utilization
  - Lighter moving weight on extruder and so head can change direction easily and quickly
  - Prints are not shaken constantly, since build plate moves up only after one complete layer build and so more stable at the initial layer
  - Printhead does not need to carry mass of X-stepper motor. This reduces burden of the shaft, compared to designs where motor moves with axis
  - Future compatibility with tool changing systems
  - Printers can be easily enclosed
- **Cons:**
  - Complicated assembly and belt system design
  - Long belts required and so difficult to tension belts
  - X and Y cannot be driven by lead screw, only with belt
  - The frame design in CoreXY is also a source of inaccuracies. While the frame is relatively stable compared to other 3D printers, it can be inaccurate if the frame is not assembled perfectly square. However, this problem can be resolved with a fixed square. Corner brackets can also help keep the CoreXY printer permanently square.

**Some notes on CoreXY Mechanics:**
CoreXY has long belts. The belts can contribute to ringing in the prints because long belts will tend to stretch more under acceleration than shorter belts, but the ringing problem isn't nearly as bad as the ringing you get when you try to move the massive bed at print speed. Using wider belts can help keep ringing to a minimum in a CoreXY mechanism (or any other printer type).

Of course, you can always build it poorly create problems. One of the common errors I've seen in a lot of CoreXY builds is not keeping the belts parallel to the guide rails. That causes the belt tension to vary depending on the extruder carriage coordinates and results in distorted prints, and if it's bad enough, causes belts to slip on drive pulleys.

The other thing to be aware of is that when printing lines at 45 or 135 degrees, such as solid infill, only one motor is moving the entire mass of the X and Y axes. You have to choose your motors accordingly and set speed, acceleration, and junction deviation (or jerk) to values that will work with your printer. When you see people complaining of layer shifting problems in a CoreXY machine, it almost always occurs when it is printing 45 or 135 degree infill. Don't skimp on motor torque.

In a machine with the bed moving in the Y axis, the typical end-supported guide rails will flex and the bed will move in an arc instead of a straight line. In a coreXY mechanism if you use end-supported guide rails you can have the same problem, usually to a lesser extent because of the lower moving mass in the CoreXY. It's pretty easy to build a CoreXY mechanism using fully supported linear guides in the Y axis and even in X and have very few problems with rails sagging.

In the CoreXY design, the tension of the belts plays a major role. For example, if the belts are selected from inferior material, they can stretch more quickly and therefore wear out more quickly. This often causes the 3D printer to lose accuracy after a short time. 

4.	**H-bot** printers are similar to Core XY with the biggest difference being that CoreXY systems have two timing belts and H-Bot systems have only one. This means that CoreXY systems have less vibration and can print much more accurately and quicker than H-Bot systems.

Pros:
- Only one belt 
- Purely theoretically a very high precision can be achieved

 More on the differences between CoreXY and Hbot [here](https://the3dprinterbee.com/corexy-vs-hbot/#:~:text=How%20do%20CoreXY%20and%20H,quicker%20than%20H%2DBot%20systems.)

5. **D-bot**

6. **Crossing gantry** or QuadRap mechanism like in the Ultimaker