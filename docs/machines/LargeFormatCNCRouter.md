# Large Format CNC Router

1) [PrintNC](https://wiki.printnc.info/en/home) Large CNC Router - Fully Open Source CNC Router build with great Wiki having full assembly instructions
- Complete BOM, CAD and instructions to build.
![PrintNC Picture](https://wiki.printnc.info/printnc-v3.png)
Specifications:
- Cutting Area: 1000 mm x 600 mm (Build size can customized)
- Materials: Can cut Wood, plastics and Aluminium
- Linear Motion: Ballscrews with BFBK bearing mounts
- VFD water cooled spindle
- GRBL 

2) 3D printer CNC Rounter from [TopsCNC](https://createforcuriosity.com/fr/design/4A8e0Mgv/details) is an open source CNC router design with all fittings 3D printed. 
- 