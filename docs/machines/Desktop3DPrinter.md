# Desktop 3D Printer


## Some Popular Open Source & Commercial 3D Printers
Preliminary research was carried out to see which open source designs and commercial 3D are popular in the 3D printer community. These will be used as a starting point for the development.

Various repositories such as Youtube, Github, Instructables, GrabCad, Thingiverse, Personal Blogs and Technical Forums were searched.

### Popular Open Source/Kit 3D Printers

1. The [Prusa i3MK3](https://www.prusa3d.com/) is one of the most popular open source 3D printers which consistently ranks among the [best printers](https://all3dp.com/1/best-3d-printer-reviews-top-3d-printers-home-3-d-printer-3d/) money can buy. The printer currently costs about 800 Euros which is slightly on the pricey end.

![Prusa i3MK3](https://i.all3dp.com/cdn-cgi/image/fit=cover,w=1000,gravity=0.5x0.5,format=auto/wp-content/uploads/2019/03/15155153/prusa6.jpg)
   
   Observing the build quality of several Prusa printers built in our Open Lab at the HSU, the build isn't one of the sturdiest out there. One possible upgrade to increase the frame strength and make the printer more sturdy is the [bear upgrade](https://github.com/gregsaun/prusa_i3_bear_upgrade), which is an all aluminium frame upgrade for the Prusa i3MK3 costing about 80 Euros bought as a kit. Moreover, most parts cannot be sourced locally, i.e. they **have** to be bought from Prusa. 

<img src= "https://orballoprinting.com/827-thickbox_default/prusa-bear-full-upgrade-mk3.jpg" alt="bear upgrade" width="500" height="500">

2. The [Vulcaman V1 Reprap 3D-Printer](https://www.instructables.com/Vulcanus-V1-3D-Printer/) with a build cost of 300€ is a [CoreXY](http://corexy.com/theory.html) fully DIY 3D printer based on the reprap model. It has a fully enclosed design and the design is licensed under Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
   - BOM, CAD and a detailed instruction manual are available.
   - Much interaction from community on the design and 15+ rebuilds which shows the design is relatively simple with good instructions and is reproducible.

<img src="https://content.instructables.com/ORIG/F7W/NLHN/I9IBO9VQ/F7WNLHNI9IBO9VQ.jpg?auto=webp&frame=1&height=1024&fit=bounds&md=bd7018083287778af70e2362185266ae" alt="Vulcaman V1 Reprap Model" width="450" height="450" style="vertical-align:middle">

    Specifications:
   - Dimensions: 44cm x 44cm x 60cm
   - Build Volume: 20cm x 20cm X 26cm
   - Travel Speed: 300mm/s
   - Resolution: up to 0.05mm
   - Electronic: Ramps 1.4 with TMC2100 1/256 microstep Motordriver

3. The [Falla 3D](http://www.falla3d.com/index_en.html) is an OpenSource 3D Printer with a magnetic levitation system for the bearings of the X and Y axes. The printer is also modular (it can extrude all 3mm and 1.75 mm filaments) and is scalable.
   - BOM and CAD files exist on [github](https://github.com/3dita/Falla3D)
  
<img src="http://www.falla3d.com/images/screenshots/45dx.jpg" width="350" height="350" style="vertical-align:middle">

 - Specifications:
   - Scalable up to a 90x60x60 cm printing area (Printer size 100x70x70 cm)
   - Double Extruder - With a single FUM hotend, with a classic double 3mm hotend or with a 1.75 Bowden system.

4. The [D-bot Core XY 3D printer](https://www.thingiverse.com/thing:1001065) from user Spauda01 on Makerbot's Thingiverse is another solid alu frame design with a coreXY mechanism. The design has high community interaction and over 140 replications. 
- BOM, CAD and Build instructins (video and pdf guide) available
- Costs about 550$

<img src="http://i.imgur.com/oFbBbEb.jpg" width="350" height="350" style="vertical-align:middle">

 - Build Volume: 300mm x 200mm x 325mm
 - Licences under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) i.e. commercial as well.

5.	[Voron v2.4](https://vorondesign.com/) by Paul Nokel is a high end DIY 3D Printer that is fully open source and is popular for having excellent print quality and speed along with intuitive assembly documentation. The Voron 2.1 uses mains current to heat the stationary bed, and 12v everywhere else. 

![Voron 2.4](https://www.elektrifiziert.net/index.php?attachment/3257-2020-11-18-06h27-57-png/)
- [User buildlog](https://www.elektrifiziert.net/forum/index.php?thread/134-voron-2-4-3d-drucker-selbstbau-tagebuch/)
- 

6.	[Hypercube 3D Printer](https://www.thingiverse.com/thing:1752766) 

8.	[Hypercube Evolution](https://www.thingiverse.com/thing:2254103) (also known as the HEVO), developed by SCOTT_3D, is an iteration on the HyperCube 3D printer designed by Tech2C. It adds 3030 extrusions around the frame for added thickness, as well as a few more upgrades. It has a decent Wiki as well as a couple of different BOM generators, online and in Excel format. The design comes with configurable CAD files for required build volume and single or double Z axis motors. 

- [HEVO build guide](https://bestin-it.com/how-to-build-best-3d-printer-from-scratch-hypercube-evolution/) from a german maker detailing his build and sourcing of parts
- [Fusion Model from user1](https://myhub.autodesk360.com/ue28de06e/g/shares/SH7f1edQT22b515c761edd5edf41a5b4aa05?viewState=NoIgbgDAdAjCA0IDeAdEAXAngBwKZoC40ARXAZwEsBzAOzXjQEMyzd1C0B2CAEwCY%2BjGADMAtAGYIAI1yiALOMZ9RATjkA2FfIiMe43Ll25xAYzQBfEAF0gA&fbclid=IwAR2G9gEP79uHXTaEnc1MUpbw_OhZn3VWZfzaTZHgxxt_8QGHX8jGW89UFPA)
- [Fuson360 Model from User2](https://myhub.autodesk360.com/ue28de06e/g/shares/SH7f1edQT22b515c761e627c356ab003cc8e?viewState=NoIgbgDAdAjCA0IDeAdEAXAngBwKZoC40ARXAZwEsBzAOzXjQEMyzd1C0B2CAEwCY%2BjGADMAtAGYIAI1yiALOMZ9RATjkA2FfIiMe43Ll25xAYzQBfEAF0gA)
- 

1. [Hypercube Evolution Oliver RT or HevORT](https://miragec79.github.io/HevORT/) is a remix **high end** DIY 3D Printer based on the Hypercube Evolution design which can cost about 2000 USD. The printer has auto-levelling of bed using 3 extra z-axis motors. It has a rigid Gantry for achieving high accelerations and speed using a direct drive extruder, whereby the user has achieved a build speed of 500mm/s+ with the printer.
- [Github repository](https://github.com/MirageC79/HevORT) and website has printer configurator, BOM generator and fusion 360 CAD model.
    
10. 

<u> **Some Commercial Open Source (DIY) Desktop Printers** </u>

1. Ultimaker - 
2. Creality Ender 3


---
## Choosing a 3D Printer for the OLSK
The Open lab 3D Printer must be a reliable machine with quality components that can be built from scratch with having to source as few components as possible. The printer can be built locally without having to purchase an expensive Kit or fully assembled 3D Printer. The printer should make it possible for users to choose their own components for key elements depending on local availability of components and budget.

With quality components and systems being more expensive, 3 designs categories are proposed depending on the user budget. These are namely a high end model, a mid-consumer model and a budget model.

Using **MoSCoW** method of prioritization to prioritize the design objectives.
- **Must have (M)** — these are critical and must be included into the product. If even one isn’t included, the release is considered a failure. These can be downgraded if there’s agreement among stakeholders.
- **Should have (S)** — these requirements are important but not crucial for the release. They’re the first level of “Nice to have”, and generally share the importance of MUST requirements, without being so time-sensitive.
- **Could have (C)** — these requirements are desirable but not necessary for the release. They’re usually low-cost enhancements to the product. Due to their lower importance, they’re the second level of “Nice to have” features.
- **Won’t have (W)** — these are considered to be the least-critical or even not aligned with the product strategy. They are to be definitely dropped or to be reconsidered for future releases.

### 3D Printer Design Goals:
1. Enclosed and Stiff construction
2. Reliable and repeatable print quality
3. As much as possible can be built or sourced locally
4. Configurable based on local resources
5. No reliance on any manufacturer or proprietary hardware

### Technical Specifications
- Build Volume :- 200x200x200 to 400x400x400 (Configurable?)
- Built in Enclosure
- Enclosed ventilated electronics chamber
- Automatic bed levelling feature with inductive sensor
- Heated Build Platform (> 60 deg C?)
- High Quality hot end (E3D V6 or better)
- Use a direct-drive extruder (to print flexible filaments) / Bowden Extruder
- Swappable Nozzle (0.25, 0.4, 0.6, 0.8)
- PLA, ABS and other std. materials
- Detect if filament finishes mid build or gets stuck
- Detect power loss and resume build
- Removable build plate (flex to remove part)
- HEPA Filter with Fan to remove fumes (for ABS printing)
- Emergency Stop Button
- 32 bit control capability
- Linear guides for fast accelerations
- Wifi Connectivity
- IP Cam to see build in real time
  
## Design Categories
1. **Design 1 High End**: (Similar or cheaper than Ultimaker < 2500€ )
   - High end 3D printer with profesional grade print quality and speed
   - Durable and high end components
   - Latest features
2. **Design 2 - Mid Consumer** (similar in price to Prusa i3 < 760€ )
   - Higher end 3D printer with most key advanced features that could be in the price range of the prusa i3
3. **Design 3 - Affordable** (Similar in price to Ender 3 < 200€ )
   - 3D printer that is affordable in the range of the cheapest commercial good quality printer like the popular Creality Ender 3
   - This could include similar design considerations in terms of components and mechanics
   - Fully DIY so it can be built by anyone and almost anywhere in the world
   - Should have all the very basics to allow someone to 3D print good parts

### Current 3D Printer Design Log

For the outerframe of the design, I am starting with the Hypercube Evoution design which has a boxed construction that has been tried and tested for rigidity and stiffness. The boxed frame is also easier to enclose which is necessary for printing ABS that requires a controlled temperature environment for good layer adhesion. The design also uses the CoreXY mechanism wwhereby the motors for the X and Y axis are fixed to the frame, which makes the X carriage that carries only the print head lighter than a conventional Cartesian design which has to carry the X axis motor. This should allow higher accelerations and printing speed.

Scott3D the original creator of the Hypercube Evolution design has uploaded a parametrized and configurable CAD file in Autodesk Inventor. This needs some modifications since the parameters don't work correctly in the original file. This is a good starting point but later designs will be carried out in Fusion 360. For now a build volume of 300cm3 will be used to build the first iteration. The current design with two / motors

Part of the design criteria is to fit the power supply and electronics under the base. This means a taller machine but it has a clean look with increased safety by keeping electronics out of reach of (kids). Moreover, transport is easier and the printer can be easily placed on a desk. The board I haven't decided on a mounting solution for yet. It may live under the machine as well with a fan ducted to outside the frame but I'm still working on the basic mechanical design so I haven't gotten to the electronics yet. The frame is being designed with the intent of closing it off so I can print with whatever I want. I haven't decided on hinged or fully removable panels yet though.