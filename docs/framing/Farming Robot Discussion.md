#Farm Robot


## Requirements

- Must be Open Source.
- Affordable
- Robust
- Easy to use
- Easy to repair


## Open questions

- Do we want something for small/medium scale or large scale?
- To be used inside the city? Or at farms?
- For Interior or exterior or both
- Which areas of farming do we want to automate?
    - Planting
    - Monitoring
    - Irrigating
    - Pest control
    - Pruning
    - Weeding
    - Harvesting


## Considerations

- “According to FAO (2019b), about 90 percent of farmers worldwide operate on a small 
scale and the technology must become accessible to this large group.“  
 __FAO Agriculture 4.0*__

- “These new platforms tend to be very sophisticated and new types of equipment are 
continuously being developed; however, simple agrobots designed for basic, 
straightforward tasks can already help farmers with a wide range of operations.“  
__FAO Agriculture 4.0*__

- “Small robots at an affordable price for purchase or hire represent a potential 
alternative in areas where manpower  is scarce and conventional machinery is not 
available or is too costly for smallholders.”  
__FAO Agriculture 4.0*__

- “Agrobots can be designed to enable spare parts to be obtained via 3D printing, 
enabling decentralized production  and facilitating the related logistic.”
__FAO Agriculture 4.0*__

- Harvesting is one of the most labor intensive agricultural activities but also 
one of the most difficult to automate.  


\* <http://www.fao.org/policy-support/tools-and-publications/resources-details/en/c/1365039/>


## Key words

- Precision Agriculture
- Agriculture 4.0
- IoT
- AI/ML
- Automation
- Agrobot
- Sustainable Development Goals (SDGs)
- Produce more with less (resources)



## Inspirations

### Robot car for open-field operations

Probably one of the most popular systems in the market today.
 
Examples: 
 
- _IP-Farmrobot_  
Open Source?  
Link: <https://www.youtube.com/watch?v=tQk7wucq3jM>
  
- _FarmDroid_  
Non-Open Source  
Link: <https://farmdroid.dk/en/welcome/>  
    
    
Pros:
- Open field -> Not limited to small constrained areas
- Horizontally scalable (more robots, more production)
  
Cons:
- Complex: Autonomous driving, uneven ground, changing ground
- Batteries: Pollution, limited lifetime
- Not much open documentation -> Lots of self development/research needs to be done


### Robot swarm

Examples:
  
- _Prospero: Robotic Farmer_  
Similar to the Robot Cars, but on legs instead of wheels.    
Open Source?  
Link: <https://wiki.opensourceecology.org/wiki/Prospero:_Robotic_Farmer>

Pros:
- "Horizontally" scalable
- Swarm behaviour “smart” (also possible for robot cars)

Cons:
- Complex
- Batteries 
- Irrigation and harvesting are more complicated


### CNC Robot for planting bed

Examples:
  
- _FarmBot_    
Open Source project    
Link: <https://farm.bot/pages/open-source>


Pros:
- More controlled conditions
- Easier to automate extra steps like harvesting
- No batteries needed if there’s access to an electric outlet
- Irrigation
- Monitoring
- Weeding
- Fully documented example (Farmbot)
- All year farming if in controlled interior

Cons:
- Limited work area (How difficult would it be to modify it to be “infinite” bed length?)
- Not very scalable beyond production for family needs
- Expensive for vegetable yield (maybe it is too “hight quality” for the purpose?) -> Maybe a “downgraded” version that is less precise but way cheaper could be an option.


### Circular farming robot

Examples:

- _Agrokruh_  
Kind of Open Source but no much documentation available.  
Link: <https://wiki.opensourceecology.org/wiki/Agrokruh>


Pros:
- Scalable
  - "Horizontally" with more units
  - "Vertically" if implemented the idea of moving arm through the circles.
- Simple
- Similar to FarmBot but for larger scales

Cons:
- Only makes sense at larger scales (Not sure if a con)
- Outside the city -> Transportation of products
- More dependent on climate conditions (No winter farming)


## Ideas

- Something similar to FarmBot but vertical?
  - With soil to plant tubers or other “big” vegetables/fruits
  - Or hydroponic for greens/strawberries/Bell peppers/herbs
  - Maybe lower levels can be used to plant vegetables that hang to the ground (Like FarmBot suggests at the ends of the bed)
    
- For open-filed operations there is the concept of RTK-GPS which is high precision GPS positioning (1cm).   
Tutorial: https://learn.sparkfun.com/tutorials/what-is-gps-rtk/all This company uses that technic https://farmdroid.dk/en/welcome/
 


## Comments

In case of going for large scale farming:
Probably we should not try to solve the agricultural problems we think are important.
Although we can do our research on the main activities to automate it will always be 
important to talk to the end-user/consumer of these new technologies so we can design 
the correct solution for their problems.

Talking, understanding and engaging with the farmers and their needs is crucial for 
the adoption of new technologies, specially in the agricultural sector that has not 
seen much change for generations.
